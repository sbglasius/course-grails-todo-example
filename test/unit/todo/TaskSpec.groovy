package todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Task)
class TaskSpec extends Specification {

    def setup() {
        mockForConstraintsTests(Task)
    }

    void "test not nullable on title"() {
        when:
        def task = new Task()

        then:
        !task.save()

        and:
        task.errors['title'] == "nullable"
    }

    void "test maxSize on description"() {
        when:
        def task = new Task(title: "Title", description: "*" * 1001)

        then:
        !task.save()

        and:
        task.errors['description'] == "maxSize"
    }

    void "test validator on deadline with new object"() {
        when:
        def task = new Task(title: "Title", deadline: new Date() - 10)

        then:
        !task.save()

        and:
        task.errors['deadline'] == 'deadline.before.today'
    }

    void "test validator on deadline with existing object"() {
        setup:
        def task = new Task(title: "Title", deadline: new Date()+1).save(failOnError: true)

        when:
        task.deadline = new Date() - 10

        then:
        task.save(failOnError: true)
    }

}
