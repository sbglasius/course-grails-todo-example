package todo

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(DeadlineTagLib)
@Mock(Task)
class DeadlineTagLibSpec extends Specification {

    @Shared today = new Date().clearTime()

    @Unroll
    void "test something"() {
        expect:
        applyTemplate('<t:deadline date="${date}" format="${format}"/>', [date: date, format: format]) == expected

        where:
        date       | format       | expected
        today + 15 | 'yyyy-MM-dd' | """<span style="">${(today + 15).format('yyyy-MM-dd')}</span>"""
        today + 5  | 'yyyy-MM-dd' | """<span style="color: darkorange; font-style: italic;">${(today + 5).format('yyyy-MM-dd')}</span>"""
        today - 1  | 'yyyy-MM-dd' | """<span style="color: darkred; font-weight: bold;">${(today - 1).format('yyyy-MM-dd')}</span>"""
    }
}
