package todo

import org.springframework.context.MessageSourceResolvable


public enum Urgency implements MessageSourceResolvable {
    URGENT, NOT_URGENT

    String[] getCodes() {
        return ["todo.Urgency.${name()}"] as String[]
    }

    Object[] getArguments() {
        return new Object[0]
    }

    String getDefaultMessage() {
        return name()
    }
}
