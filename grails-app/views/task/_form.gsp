<%@ page import="todo.Task" %>



<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="task.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${taskInstance?.title}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="task.description.label" default="Description" />
		
	</label>
	<g:textArea name="description" cols="40" rows="5" maxlength="1000" value="${taskInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'priority', 'error')} required">
	<label for="priority">
		<g:message code="task.priority.label" default="Priority" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="priority" type="number" min="1" max="10" value="${taskInstance.priority}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'deadline', 'error')} ">
	<label for="deadline">
		<g:message code="task.deadline.label" default="Deadline" />
		
	</label>
	<g:datePicker name="deadline" years="[2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020]" precision="day"  value="${taskInstance?.deadline}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'importance', 'error')} ">
	<label for="importance">
		<g:message code="task.importance.label" default="Importance" />
		
	</label>
	<g:select name="importance" from="${todo.Importancy?.values()}" keys="${todo.Importancy.values()*.name()}" value="${taskInstance?.importance?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'urgency', 'error')} ">
	<label for="urgency">
		<g:message code="task.urgency.label" default="Urgency" />
		
	</label>
	<g:select name="urgency" from="${todo.Urgency?.values()}" keys="${todo.Urgency.values()*.name()}" value="${taskInstance?.urgency?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'group', 'error')} ">
	<label for="group">
		<g:message code="task.group.label" default="Group" />
		
	</label>
	<g:select id="group" name="group.id" from="${todo.Group.list()}" optionKey="id" value="${taskInstance?.group?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'person', 'error')} required">
	<label for="person">
		<g:message code="task.person.label" default="Person" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="person" name="person.id" from="${todo.Person.list()}" optionKey="id" required="" value="${taskInstance?.person?.id}" class="many-to-one"/>
</div>

