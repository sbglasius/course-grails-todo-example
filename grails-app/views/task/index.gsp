
<%@ page import="todo.Task" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'task.label', default: 'Task')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-task" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-task" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="title" title="${message(code: 'task.title.label', default: 'Title')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'task.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="priority" title="${message(code: 'task.person.label', default: 'Person')}" />
					
						<g:sortableColumn property="deadline" title="${message(code: 'task.deadline.label', default: 'Deadline')}" />
					
						<g:sortableColumn property="importance" title="${message(code: 'task.importance.label', default: 'Importance')}" />
					
						<g:sortableColumn property="urgency" title="${message(code: 'task.urgency.label', default: 'Urgency')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${taskInstanceList}" status="i" var="taskInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${taskInstance.id}">${fieldValue(bean: taskInstance, field: "title")}</g:link></td>
					
						<td>${fieldValue(bean: taskInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: taskInstance, field: "person")}
                        <g:if test="${user.person == taskInstance.person}">*</g:if>
                        </td>
					
						<td><g:formatDate date="${taskInstance.deadline}" /></td>
					
						<td>${fieldValue(bean: taskInstance, field: "importance")}</td>
					
						<td>${fieldValue(bean: taskInstance, field: "urgency")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${taskInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
