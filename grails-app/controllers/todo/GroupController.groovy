package todo

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class GroupController {
    static scaffold = true
}
