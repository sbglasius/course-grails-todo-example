import todo.*

class BootStrap {

    def init = { servletContext ->
        def g1 = new Group(name: "IT Department").save(failOnError: true)
        def g2 = new Group(name: "Human Resources").save(failOnError: true)
        new Group(name: "Finance").save(failOnError: true)

        def p1 = new Person(name: "Søren").save(failOnError: true)
        def p2 = new Person(name: "Karsten").save(failOnError: true)
        def p3 = new Person(name: "Bjarke").save(failOnError: true, flush: true)

        5.times {
            new Task(title: "Task $it", person: p1, group: g1).save(failOnError: true)
        }
        5.times {
            new Task(title: "Assignment $it", person: p2, group: g1).save(failOnError: true)
        }
        5.times {
            new Task(title: "Job $it", person: p3, group: g1).save(failOnError: true)
        }

        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)

        def adminUser = new User(username: 'admin', password: 'password', person: p1)
        adminUser.save(flush: true)

        UserRole.create adminUser, adminRole, true

        def testUser = new User(username: 'user', password: 'password', person: p1)
        testUser.save(flush: true)

        UserRole.create testUser, userRole, true
    }

    def destroy = {
    }
}
