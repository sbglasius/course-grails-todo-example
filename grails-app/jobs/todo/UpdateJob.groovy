package todo



class UpdateJob {
    static triggers = {
      simple repeatInterval: 25000l // execute job once in 5 seconds
    }

    def execute() {
        println "I was executed @ ${new Date()} !"
    }
}
