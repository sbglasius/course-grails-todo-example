package todo

class DeadlineTagLib {
    static namespace = "t"
    static defaultEncodeAs = 'html'
    static encodeAsForTags = [deadline: 'raw']

    def deadline = { attrs ->
        def date = attrs.date
        def threshold = attrs.remove('threshold') as Integer ?: 10

        if(!date) {
            throwTagError("[t:deadline] missing attribute [date]")
        }
        def styleWarn = attrs.remove('warning') ?: "color: darkorange; font-style: italic;"
        def styleOver = attrs.remove('overdue') ?: "color: darkred; font-weight: bold;"
        def today = new Date().clearTime()

        def style = ''
        if(date < today+threshold && date >= today) {
            style = styleWarn
        } else if(date < today) {
            style = styleOver
        }
        out << """<span style="$style">${g.formatDate(attrs)}</span>"""
    }

}
