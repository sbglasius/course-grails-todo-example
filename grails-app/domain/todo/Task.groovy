package todo

import grails.plugin.springsecurity.annotation.Secured

class Task {
    String title
    String description
    int priority = 1
    Date deadline
    Importancy importance
    Urgency urgency

    Group group

    static belongsTo = [person: Person]

    static constraints = {
        title blank: false
        description nullable: true, blank: true, maxSize: 1000
        priority min: 1, max: 10
        deadline nullable: true, attributes: [years: 2013..2020], validator: { value, object ->
            if(object.id == null  && value?.before(new Date().clearTime())) {
                "deadline.before.today"
            }
        }
        importance nullable: true, attributes: [valueMessagePrefix:'todo.importance']
        urgency nullable: true, attributes: [valueMessagePrefix:'todo.urgency']
        group nullable: true
    }
}
