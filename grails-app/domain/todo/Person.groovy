package todo

class Person {
    String name
    String toString() {
        name
    }

    static hasMany = [tasks: Task]
    static constraints = {
    }

//    static mapping = {
//        tasks cascade: "all-delete-orphan"
//    }
}
