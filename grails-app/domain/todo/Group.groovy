package todo


class Group {
    String name

    String toString() {
        name
    }
    Set<Task> getTasks() {
        Task.findAllByGroup(this)
    }
    static transients = ['tasks']

    static mapping = {
        table name: "COMPANY_GROUPS"
    }
}

